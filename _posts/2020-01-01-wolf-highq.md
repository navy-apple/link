---
layout: redirect
title: Redirect
description: ''
permalink: /wolf/highq
lang: ko
lang_index: 0
meta:
  title: 하이큐!! | 늑대닷컴
  desc: 배구에 매료되어 중학생 시절 최초이자 마지막 공식전에 출전한 히나타 쇼요. 하지만 코트 위의 제왕이라는 별명을 가진 천재 선수 카게야마에게 처참히 패하고 만다.
  link: 'https://olink.netlify.app/webtoon/wfwf'
  img: '/assets/images/highq-link.png'
  domain: wfwf.com
---
