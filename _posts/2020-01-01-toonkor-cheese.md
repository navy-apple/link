---
layout: redirect
title: Redirect
description: ''
permalink: /toonkor/cheese
lang: ko
lang_index: 0
meta:
  title: 치즈인더트랩 | 툰코
  desc: 평범한 여대생 홍설, 그리고 어딘가 수상한 선배 유정.미묘한 관계의 이들이 펼쳐나가는 이야기.
  link: 'https://olink.netlify.app/webtoon/tkor'
  img: '/assets/images/cheese-in-the-trap.png'
  domain: tkor.com
---
