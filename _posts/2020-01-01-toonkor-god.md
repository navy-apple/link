---
layout: redirect
title: Redirect
description: ''
permalink: /toonkor/god
lang: ko
lang_index: 0
meta:
  title: 신의 탑 | 툰코
  desc: 자신의 모든 것이었던 소녀를 쫓아 탑에 들어온 소년그리고 그런 소년을 시험하는 탑.
  link: 'https://olink.netlify.app/webtoon/tkor'
  img: '/assets/images/towerofgod-link.png'
  domain: tkor.com
---
