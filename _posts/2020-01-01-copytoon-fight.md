---
layout: redirect
title: Redirect
description: ''
permalink: /copytoon/fight
lang: ko
lang_index: 0
meta:
  title: 싸움독학 | 카피툰
  desc: 힘 없고 가난한 내가 우연히 발견한 비밀의 뉴투부. 그건 싸움을 가르치는 뉴투부였다.
  link: 'https://olink.netlify.app/webtoon/copy'
  img: '/assets/images/fighting-alone.png'
  domain: copytoon.com
---
