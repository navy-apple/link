---
layout: redirect
title: Redirect
description: ''
permalink: /toonkor/princess
lang: ko
lang_index: 0
meta:
  title: 어느 날 공주가 되어버렸다 | 툰코
  desc: 어느 날 눈을 떠보니 공주님이 되었다!금수저를 입에 물고 태어난 건 좋은데,하필이면 친아버지의 손에 죽는 비운의 공주라니!
  link: 'https://olink.netlify.app/webtoon/tkor'
  img: '/assets/images/been-princess-link.png'
  domain: tkor.com
---
