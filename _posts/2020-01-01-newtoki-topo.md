---
layout: redirect
title: Redirect
description: ''
permalink: /newtoki/topo
lang: ko
lang_index: 0
meta:
  title: 외모지상주의 > 뉴토끼
  desc: 어느날 그에게 일어난 기적같은 일
  link: 'https://olink.netlify.app/webtoon/newtoki'
  img: '/assets/images/topography-link.png'
  domain: newtoki.com
---
