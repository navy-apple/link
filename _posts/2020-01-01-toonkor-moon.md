---
layout: redirect
title: Redirect
description: ''
permalink: /toonkor/moon
lang: ko
lang_index: 0
meta:
  title: 낮에 뜨는 달 | 툰코
  desc: 시간이 멈춘 남자와 흘러가는 여자. 과거와 현재를 오가는 갈등에 대한 이야기.
  link: 'https://olink.netlify.app/webtoon/tkor'
  img: '/assets/images/moon-rising-daytime-link.png'
  domain: tkor.com
---
