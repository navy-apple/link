---
layout: redirect
title: Redirect
description: ''
permalink: /toonkor/love
lang: ko
lang_index: 0
meta:
  title: 연애혁명 | 툰코
  desc: 로맨스, 그런 건 우리에게 있을 수가 없어! 신개념 개그 로맨스.
  link: 'https://olink.netlify.app/webtoon/tkor'
  img: '/assets/images/love-revolution-link.png'
  domain: tkor.com
---
