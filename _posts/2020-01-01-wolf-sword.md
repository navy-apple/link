---
layout: redirect
title: Redirect
description: ''
permalink: /wolf/sword
lang: ko
lang_index: 0
meta:
  title: 귀멸의 칼날 | 늑대닷컴
  desc: 절망을 끊고, 칼날이 되어라!
  link: 'https://olink.netlify.app/webtoon/wfwf'
  img: '/assets/images/killing-ghost-sword.png'
  domain: wfwf.com
---
