---
layout: redirect
title: Redirect
description: ''
permalink: /copytoon/level
lang: ko
lang_index: 0
meta:
  title: 나 혼자만 레벨업 | 카피툰
  desc: 10여 년 전, 다른 차원과 이쪽 세계를 이어 주는 통로 ‘게이트’가 열리고 평범한 이들 중 각성한 자들이 생겨났다.게이트 안의 던전에서 마물을 사냥하는 각성자.그들을 일컬어 ‘헌터’라 부른다.
  link: 'https://olink.netlify.app/webtoon/copy'
  img: '/assets/images/level-up-alone-link.png'
  domain: copytoon.com
---
