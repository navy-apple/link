---
layout: redirect
title: Redirect
description: ''
permalink: /toonkor/mart
lang: ko
lang_index: 0
meta:
  title: 쌉니다 천리마마트 | 툰코
  desc: 고객은 왕이 아니다, 직원이 왕이다!"병맛 드라마를 뛰어넘는 미친 상상력의 원작 웹툰
  link: 'https://olink.netlify.app/webtoon/tkor'
  img: '/assets/images/cheonrima-link.png'
  domain: tkor.com
---
